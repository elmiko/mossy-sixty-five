/*
    Copyright (C) 2023  Michael McCune

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use crate::prelude::*;

// 64KiB of memory
const MEMORY_SIZE_BYTES: usize = 1 << 16;

// When reset, the program counter should be loaded with the contents from the
// following locations.
// Low order byte for program counter is at this address.
const RESET_PCL_ADDRESS: u16 = 0xfffc;
// High order byte for program counter is at this address.
const RESET_PCH_ADDRESS: u16 = 0xfffd;

// The stack starts at 0x01ff and ends at 0x0100
// The end address of the stack.
const STACK_END: u8 = 0x00;
// The start address of the stack.
const STACK_START: u8 = 0xff;

/// Cpu represents a 6502-compatible CPU state with 64KiB memory.
#[derive(Clone, Debug, PartialEq)]
pub struct Cpu {
    pc: u16,
    ac: u8,
    x: u8,
    y: u8,
    sr: u8,
    sp: u8,
    memory: [u8; MEMORY_SIZE_BYTES],
}

impl Cpu {
    pub fn new() -> Self {
        Cpu {
            pc: 0,
            ac: 0,
            x: 0,
            y: 0,
            sr: 0,
            sp: STACK_START,
            memory: [0; MEMORY_SIZE_BYTES],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_cpu_has_default_pc() {
        let obs = Cpu::new();
        assert_eq!(obs.pc, 0);
    }

    #[test]
    fn new_cpu_has_default_ac() {
        let obs = Cpu::new();
        assert_eq!(obs.ac, 0);
    }

    #[test]
    fn new_cpu_has_default_x() {
        let obs = Cpu::new();
        assert_eq!(obs.x, 0);
    }

    #[test]
    fn new_cpu_has_default_y() {
        let obs = Cpu::new();
        assert_eq!(obs.y, 0);
    }

    #[test]
    fn new_cpu_has_default_sr() {
        let obs = Cpu::new();
        assert_eq!(obs.sr, 0);
    }

    #[test]
    fn new_cpu_has_default_sp() {
        let obs = Cpu::new();
        assert_eq!(obs.sp, STACK_START);
    }

    #[test]
    fn new_cpu_has_default_memory() {
        let obs = Cpu::new();
        let exp: [u8; MEMORY_SIZE_BYTES] = [0; MEMORY_SIZE_BYTES];
        assert_eq!(obs.memory, exp);
    }
}
